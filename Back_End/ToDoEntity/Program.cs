﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoEntity
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new ToDoListEntities();
            var Category = new Category()
            {
                Name = "Terraria",
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
                

            };

            var Task = new Task()
            {
                Name = "Farm Nazar",
                Time = new DateTime(2018, 3, 28, 15, 30, 0),
                Category = "Terraria",
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
            };
            context.Tasks.Add(Task);
            context.SaveChanges();
        }
    }
}
