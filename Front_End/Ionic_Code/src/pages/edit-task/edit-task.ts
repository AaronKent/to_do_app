import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { AddCategoryPage } from '../add-category/add-category';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { DatabaseServiceProvider } from '../../providers/database-service/database-service';

/**
 * Generated class for the EditTaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-task',
  templateUrl: 'edit-task.html',
})
export class EditTaskPage {
  task = { Time: '', CategoryName: '', Name: '', TaskID: '' };
  editedTask: any;
  categories: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public mdlCtrl: ModalController, public dbService: DatabaseServiceProvider,public viewCtrl: ViewController) {
    console.log(navParams.data.Name);
    this.getCategories();

  }
  /**
   * Launches page to add a new category
   */
  launchAddCategoryModal() {
    let modal = this.mdlCtrl.create(AddCategoryPage);

    modal.present();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad EditTaskPage');
  }
  /**
   * Gets array of categories to display to the user to select from
   */
  getCategories() {
    this.dbService.getCategories()
      .then(data => {
        console.log(data)
        this.categories = data;
      });
  }
  /**
   * Checks to see if any empty fields and gets them from the existing task then the provider sends a
   * PUT request
   */
  editTask() {
    if (this.task.Time == '') {
      this.task.Time = this.navParams.data.Time;
    }
    if (this.task.CategoryName == '') {
      this.task.CategoryName = this.navParams.data.CategoryName;
    }
    if (this.task.Name == '') {
      this.task.Name = this.navParams.data.Name;
    }
    this.task.TaskID = this.navParams.data.TaskID;
    
    this.dbService.editTask(this.task).then((result) => {
      console.log(result);
    }, (err) => {
      console.log(err);
      });
    this.viewCtrl.dismiss();
  }
  /**
   * Closes the current page
   */
  closeModal() {
    this.viewCtrl.dismiss();
  }

}
