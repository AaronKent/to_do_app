import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, PopoverController, } from 'ionic-angular';
import { DatabaseServiceProvider } from '../../providers/database-service/database-service';
/**
 * Generated class for the AddCategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-category',
  templateUrl: 'add-category.html',
})
export class AddCategoryPage {

  category = { Name: '' };


  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public dbService: DatabaseServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddCategoryPage');
  }
  /**
   * addCategory()
   *
   * Used to add a new category to the categories table within the database
   * Sends a POST method along with the required data.
   */
  addCategory() {
    this.dbService.addCategory(this.category).then((result) => {
      console.log(result);
    }, (err) => {
      console.log(err);
      });
    this.viewCtrl.dismiss();
  }

  /**
   * Closes the current page
   */
  closeModal() {
    this.viewCtrl.dismiss();
  }

}
