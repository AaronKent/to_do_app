import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { DatabaseServiceProvider } from '../../providers/database-service/database-service';

/**
 * Generated class for the CategoryFilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-category-filter',
  templateUrl: 'category-filter.html',
})
export class CategoryFilterPage {
  selectedCategories = [];
  categories: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public dbService: DatabaseServiceProvider) {
    this.getCategories();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoryFilterPage');
  }

  closePage() {
    this.viewCtrl.dismiss(this.categories);
  }
  getCategories() {
    this.dbService.getCategories()
      .then(data => {
        console.log(data)
        this.categories = data;
      });
  }

}
