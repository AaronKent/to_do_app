import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, NavParams, ModalController  } from 'ionic-angular';
import { DatabaseServiceProvider } from '../../providers/database-service/database-service';
import { AddCategoryPage } from '../add-category/add-category';

/**
 * Generated class for the AddTaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-task',
  templateUrl: 'add-task.html',
})
export class AddTaskPage {

  task = { Time: '', CategoryName: '', Name: '' };
  categories: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public dbService: DatabaseServiceProvider, public mdlCtrl: ModalController) {
    this.getCategories();
  }


  

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddTaskPage');
  }

  
  /**
   * Saves the task to the SQL server using the information from the form
   */
  saveTask() {
    this.dbService.addTask(this.task).then((result) => {
      console.log(result);
    }, (err) => {
      console.log(err);
      });
    this.viewCtrl.dismiss();
  }
  /**
   * Used to display categories to select from on the form
   */
  getCategories() {
    this.dbService.getCategories()
      .then(data => {
        console.log(data)
        this.categories = data;
      });
  }
  /**
   * Launches page to add new category
   */
  launchAddCategoryModal() {
    let modal = this.mdlCtrl.create (AddCategoryPage);

    modal.present();
  }
  /**
   *  close current page
   */
  closeModal() {
    this.viewCtrl.dismiss();
  }

}
