import { Component } from '@angular/core';
import { NavController, ModalController, PopoverController} from 'ionic-angular';
import { DatabaseServiceProvider } from '../../providers/database-service/database-service'
import { AddTaskPage} from '../add-task/add-task'
import { CategoryFilterPage } from '../category-filter/category-filter';
import { EditTaskPage } from '../edit-task/edit-task';
import { TimeFilterPage } from '../time-filter/time-filter';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  tasks: any;
  addedtask: any;
  descending: boolean;
  timeFilters: any;

  task = { CreatedAt: "2018-03-19T14:12:50.807", CategoryName: "To-do List Progress" , Name: "Finish Post"};

  constructor(public navCtrl: NavController, public tasksGet: DatabaseServiceProvider, public mdlCtrl: ModalController, public popCtrl: PopoverController) {

    this.descending = true;
    this.getTasks();
    
  }
  /**
   *  Get Tasks from server
   */
  getTasks() {
    this.tasksGet.getTasks()
      .then(data => {
        this.tasks = data;
      });
    
  }
  /**
   * Sends request to add new task to server, along with data
   */
  addTask() {
    this.tasksGet.addTask(this.task).then((result) => {
      console.log(result);
    }, (err) => {
      console.log(err);
      });
    this.getTasks();
  }
  /**
   * Sends request to the server to delete the specified task
   * @param data
   */
  deleteTask(data) {
    this.tasksGet.deleteTask(data).then((result) => {
      console.log(result);
    }, (err) => {
      console.log(err);
    });
    this.getTasks();
  }
  /**
   * Opens a new page for adding a new task
   */
  launchAddTaskModal() {
    let modal = this.mdlCtrl.create(AddTaskPage);

    modal.present();

  }
  /**
   * Sorts current tasks based on the times of the tasks
   */
  sortTasks() {
    if (this.descending) {
      this.descending = false;
      this.tasks.sort((n1, n2) => {
        n1 = new Date(n1.Time);
        n2 = new Date(n2.Time);
        return n1 > n2 ? -1 : n1 < n2 ? 1 : 0;
      });
      console.log(this.tasks);
    }
    else {
      this.descending = true;
      this.tasks.sort((n1, n2) => {
        n1 = new Date(n1.Time);
        n2 = new Date(n2.Time);
        return n1 < n2 ? -1 : n1 > n2 ? 1 : 0;
      });
      console.log(this.tasks);
    }


  }
  /**
   * Filters tasks based on given categories returned from the popover page
   */
  categoryFilter() {
    let popover = this.popCtrl.create(CategoryFilterPage);
    popover.present();
    popover.onDidDismiss(data => {
      console.log(data);
      this.tasks = this.tasks.filter((task => {
        return (data.indexOf(task.CategoryName) > -1);
      }))
    })
  }
    /**
   * Filters tasks based on given max and min time returned from the popover page
   */
  timeFilter() {
    let popover = this.popCtrl.create(TimeFilterPage);
    popover.present();
    popover.onDidDismiss(data => {
      console.log(data);
      this.tasks = this.tasks.filter((task) => {
        return new Date(task.Time) > new Date(data.minTime) && new Date(task.Time) < new Date(data.maxTime);
      })
    })
  }
  /**
   * opens a page allowing editing the given task
   * @param data
   */
  editTask(data) {
    let modal = this.mdlCtrl.create(EditTaskPage,data);
    modal.present();

  }

}
