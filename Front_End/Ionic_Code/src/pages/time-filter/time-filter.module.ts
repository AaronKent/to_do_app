import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TimeFilterPage } from './time-filter';

@NgModule({
  declarations: [
    TimeFilterPage,
  ],
  imports: [
    IonicPageModule.forChild(TimeFilterPage),
  ],
})
export class TimeFilterPageModule {}
