import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AddTaskPage } from '../pages/add-task/add-task';
import { AddCategoryPage } from '../pages/add-category/add-category';
import { CategoryFilterPage } from '../pages/category-filter/category-filter';
import { EditTaskPage } from '../pages/edit-task/edit-task';
import { DatabaseServiceProvider } from '../providers/database-service/database-service';
import { TimeFilterPage } from '../pages/time-filter/time-filter';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AddTaskPage,
    AddCategoryPage,
    CategoryFilterPage,
    EditTaskPage,
    TimeFilterPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AddTaskPage,
    AddCategoryPage,
    CategoryFilterPage,
    EditTaskPage,
    TimeFilterPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DatabaseServiceProvider
  ]
})
export class AppModule {}
