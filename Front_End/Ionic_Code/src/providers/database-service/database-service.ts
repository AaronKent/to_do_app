import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the DatabaseServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseServiceProvider {

  apiurl = "http://localhost:50128/api";


  constructor(public http: HttpClient) {
    console.log('Hello DatabaseServiceProvider Provider');
  }

  /**
   *  Sends a GET request to get all tasks from the SQL server
   */
  getTasks() {
    return new Promise(resolve => {
      this.http.get(this.apiurl + '/tasks').subscribe(data => {
        resolve(data);

      }, err => {
        console.log(err);
      });
    });
  }
  /**
   * Sends a POST request to add a new task to the database using the data given.
   * @param data
   */
  addTask(data) {
    return new Promise((resolve, reject) => {
      this.http.post(this.apiurl + '/tasks', (data))
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          console.log(data);
          reject(err);
        });
    });
  }
  /**
   * Sends a DELETE request to the server to delete the give task
   * @param data
   */
  deleteTask(data) {
    return new Promise(resolve => {
      this.http.delete(this.apiurl + '/tasks/' + data.TaskID).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
  /**
   * Sends a GET Request to the categories API to get all categories
   */
  getCategories() {
    return new Promise(resolve => {
      this.http.get(this.apiurl + '/categories').subscribe(data => {
        resolve(data);

      }, err => {
        console.log(err);
      });
    });
  }
  /**
   * Sends a POST request to the categories API to add a new category
   * @param data
   */
  addCategory(data) {
    return new Promise((resolve, reject) => {
      this.http.post(this.apiurl + '/categories', (data))
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          console.log(data);
          reject(err);
        });
    });
  }
  /**
   * Sends a PUT request to the tasks API to edit the given task
   * @param data
   */
  editTask(data) {
    return new Promise((resolve, reject) => {
      this.http.put(this.apiurl + '/tasks/'+data.TaskID, (data))
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          console.log(data);
          reject(err);
        });
    });
  }

}
